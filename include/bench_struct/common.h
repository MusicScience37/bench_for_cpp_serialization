#pragma once

#include <cstdint>
#include <random>
#include <vector>

struct test_struct {
    std::int32_t param_int;
    double param_double;
    std::vector<char> binary;
};

inline test_struct create_data() {
    std::mt19937 engine;
    std::uniform_int_distribution<int> dist{0, 0xFF};
    std::vector<char> binary;
    constexpr std::size_t binary_size = 1024;
    binary.reserve(binary_size);
    for (std::size_t i = 0; i < binary_size; ++i) {
        binary.push_back(
            static_cast<char>(static_cast<unsigned char>(dist(engine))));
    }

    return test_struct{37, 3.14159265, std::move(binary)};
}
