#pragma once

#include <nlohmann/json.hpp>

#include "bench_struct/common.h"

namespace nlohmann {

template <>
struct adl_serializer<test_struct> {
    static void to_json(json& j, const test_struct& obj) {
        j = json{obj.param_int, obj.param_double, obj.binary};
    }

    static test_struct from_json(const json& j) {
        auto data =
            j.get<std::tuple<std::int32_t, double, std::vector<char>>>();
        return test_struct{
            std::get<0>(data), std::get<1>(data), std::move(std::get<2>(data))};
    }
};

}  // namespace nlohmann
