#pragma once

#include <random>
#include <vector>

inline std::vector<double> create_double(std::size_t size) {
    std::mt19937 engine;
    std::normal_distribution<double> dist(0.0, 1e+10);
    std::vector<double> res;
    res.reserve(size);
    for (std::size_t i = 0; i < size; ++i) {
        res.push_back(dist(engine));
    }
    return res;
}
