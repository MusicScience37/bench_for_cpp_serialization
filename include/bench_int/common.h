#pragma once

#include <random>
#include <vector>

inline std::vector<int> create_int(std::size_t size) {
    std::mt19937 engine;
    std::uniform_int_distribution<int> dist;
    std::vector<int> res;
    res.reserve(size);
    for (std::size_t i = 0; i < size; ++i) {
        res.push_back(dist(engine));
    }
    return res;
}
