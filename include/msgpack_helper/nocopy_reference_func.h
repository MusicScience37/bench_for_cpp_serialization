#pragma once

#include <cstddef>

#include <msgpack.hpp>

inline bool nocopy_reference_func(msgpack::type::object_type /*type*/,
    std::size_t /*length*/, void* /*user_data*/) {
    return true;
}
