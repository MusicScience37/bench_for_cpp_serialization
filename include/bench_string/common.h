#pragma once

#include <random>
#include <string>

inline std::string create_string(std::size_t size) {
    std::mt19937 engine;
    std::uniform_int_distribution<int> dist{0x20, 0x7E};
    std::string res;
    res.reserve(size);
    for (std::size_t i = 0; i < size; ++i) {
        res.push_back(static_cast<char>(dist(engine)));
    }
    return res;
}
