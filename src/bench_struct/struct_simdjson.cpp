#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <nlohmann/json.hpp>
#include <simdjson.h>

#include "bench_struct/common.h"
#include "bench_struct/nlohmann_serializer.h"

TEST_CASE("bench_struct_simdjson") {
    const auto data = create_data();

    // simdjson cannot create JSON string
    // (https://github.com/simdjson/simdjson/issues/821#issuecomment-620208285).
    const auto serialized_raw = nlohmann::json(data).dump();
    const auto serialized = simdjson::padded_string(serialized_raw);

    simdjson::ondemand::parser parser;
    auto deserialized_doc = parser.iterate(serialized);
    test_struct deserialized{};
    std::size_t ind = 0;
    for (auto child : deserialized_doc.get_array()) {
        if (ind == 0) {
            deserialized.param_int =
                static_cast<std::int32_t>(child.get_int64());
        } else if (ind == 1) {
            deserialized.param_double = static_cast<double>(child);
        } else {
            for (auto elem : child.get_array()) {
                deserialized.binary.push_back(
                    static_cast<char>(elem.get_int64()));
            }
        }
        ++ind;
    }
    REQUIRE(deserialized.param_int == data.param_int);
    REQUIRE(deserialized.param_double == data.param_double);
    REQUIRE(deserialized.binary == data.binary);

    BENCHMARK("deserialize struct with simdjson") {
        simdjson::ondemand::parser parser;
        auto deserialized_doc = parser.iterate(serialized);
        test_struct deserialized{};
        std::size_t ind = 0;
        for (auto child : deserialized_doc.get_array()) {
            if (ind == 0) {
                deserialized.param_int =
                    static_cast<std::int32_t>(child.get_int64());
            } else if (ind == 1) {
                deserialized.param_double = static_cast<double>(child);
            } else {
                for (auto elem : child.get_array()) {
                    deserialized.binary.push_back(
                        static_cast<char>(elem.get_int64()));
                }
            }
            ++ind;
        }
        return deserialized;
    };
}
