#include <sstream>

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>

#include "bench_struct/common.h"

template <typename Archive>
void serialize(Archive& archive, test_struct& obj) {
    archive(obj.param_int, obj.param_double, obj.binary);
}

TEST_CASE("bench_struct_cereal") {
    const auto data = create_data();

    BENCHMARK("serialize struct with cereal") {
        std::ostringstream output_stream;
        {
            cereal::BinaryOutputArchive output_archive(output_stream);
            output_archive(data);
        }
        return output_stream.str();
    };

    std::ostringstream output_stream;
    {
        cereal::BinaryOutputArchive output_archive(output_stream);
        output_archive(data);
    }
    const auto serialized = output_stream.str();

    std::istringstream input_stream;
    input_stream.str(serialized);
    test_struct deserialized;
    {
        cereal::BinaryInputArchive input_archive(input_stream);
        input_archive(deserialized);
    }
    REQUIRE(deserialized.param_int == data.param_int);
    REQUIRE(deserialized.param_double == data.param_double);
    REQUIRE(deserialized.binary == data.binary);

    BENCHMARK("deserialize struct with cereal") {
        std::istringstream input_stream;
        input_stream.str(serialized);
        test_struct deserialized;
        {
            cereal::BinaryInputArchive input_archive(input_stream);
            input_archive(deserialized);
        }
        return deserialized;
    };
}
