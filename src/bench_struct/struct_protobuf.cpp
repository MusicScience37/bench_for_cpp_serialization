#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "bench_data/bench_data.pb.h"
#include "bench_struct/common.h"

TEST_CASE("bench_struct_protobuf") {
    const auto data = create_data();

    BENCHMARK("serialize struct with protobuf") {
        bench_data::StructData buf;
        buf.set_param_int(data.param_int);
        buf.set_param_double(data.param_double);
        buf.set_binary(std::string(data.binary.begin(), data.binary.end()));
        const auto serialized = buf.SerializeAsString();
        return serialized;
    };

    bench_data::StructData pack_buf;
    pack_buf.set_param_int(data.param_int);
    pack_buf.set_param_double(data.param_double);
    pack_buf.set_binary(std::string(data.binary.begin(), data.binary.end()));
    const auto serialized = pack_buf.SerializeAsString();

    bench_data::StructData unpack_buf;
    REQUIRE(unpack_buf.ParseFromString(serialized));
    const auto deserialized =
        test_struct{unpack_buf.param_int(), unpack_buf.param_double(),
            std::vector<char>(
                unpack_buf.binary().begin(), unpack_buf.binary().end())};
    REQUIRE(deserialized.param_int == data.param_int);
    REQUIRE(deserialized.param_double == data.param_double);
    REQUIRE(deserialized.binary == data.binary);

    BENCHMARK("deserialize struct with protobuf") {
        bench_data::StructData buf;
        buf.ParseFromString(serialized);
        const auto deserialized =
            test_struct{buf.param_int(), buf.param_double(),
                std::vector<char>(buf.binary().begin(), buf.binary().end())};
        return deserialized;
    };
}
