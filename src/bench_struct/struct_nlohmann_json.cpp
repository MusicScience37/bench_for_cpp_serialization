#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <nlohmann/json.hpp>

#include "bench_struct/common.h"
#include "bench_struct/nlohmann_serializer.h"

TEST_CASE("bench_struct_nlohmann_json") {
    const auto data = create_data();

    BENCHMARK("serialize struct with nlohmann/json") {
        return nlohmann::json(data).dump();
    };

    const auto serialized = nlohmann::json(data).dump();

    const auto deserialized =
        nlohmann::json::parse(serialized).get<test_struct>();
    REQUIRE(deserialized.param_int == data.param_int);
    REQUIRE(deserialized.param_double == data.param_double);
    REQUIRE(deserialized.binary == data.binary);

    BENCHMARK("deserialize struct with nlohmann/json") {
        return nlohmann::json::parse(serialized).get<test_struct>();
    };
}
