#include <limits>

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "bench_struct/common.h"

static std::string serialize(const test_struct& data) {
    rapidjson::Document doc;
    doc.SetArray();
    doc.Reserve(3, doc.GetAllocator());
    doc.PushBack(data.param_int, doc.GetAllocator());
    doc.PushBack(data.param_double, doc.GetAllocator());
    rapidjson::Value binary_val;
    binary_val.SetArray();
    binary_val.Reserve(data.binary.size(), doc.GetAllocator());
    for (const auto byte : data.binary) {
        binary_val.PushBack(static_cast<int>(byte), doc.GetAllocator());
    }
    doc.PushBack(std::move(binary_val), doc.GetAllocator());

    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
    doc.Accept(writer);
    const auto serialized = buf.GetString();
    return serialized;
}

static test_struct deserialize(const std::string& serialized) {
    rapidjson::Document doc;
    doc.Parse(serialized.data());
    if (!doc.IsArray()) {
        throw std::runtime_error("array is required");
    }
    if (doc.Size() != 3) {
        throw std::runtime_error("array must have 3 items");
    }

    const auto& param_int_val = doc[0];
    if (!param_int_val.Is<std::int32_t>()) {
        throw std::runtime_error("int is required");
    }
    const auto param_int = param_int_val.Get<std::int32_t>();

    const auto& param_double_val = doc[1];
    if (!param_double_val.IsDouble()) {
        throw std::runtime_error("double is required");
    }
    const auto param_double = param_double_val.GetDouble();

    const auto& binary_val = doc[2];
    if (!binary_val.IsArray()) {
        throw std::runtime_error("array is required");
    }
    std::vector<char> binary;
    binary.reserve(binary_val.Size());
    for (auto iter = binary_val.Begin(); iter != binary_val.End(); ++iter) {
        if (!iter->IsInt()) {
            throw std::runtime_error("char is required");
        }
        int byte = iter->GetInt();
        if (byte < std::numeric_limits<char>::min() ||
            std::numeric_limits<char>::max() < byte) {
            throw std::runtime_error("char is required");
        }
        binary.push_back(static_cast<char>(byte));
    }
    return test_struct{param_int, param_double, std::move(binary)};
}

TEST_CASE("bench_struct_rapidjson") {
    const auto data = create_data();

    BENCHMARK("serialize struct with rapidjson") { return serialize(data); };

    const auto serialized = serialize(data);

    const auto deserialized = deserialize(serialized);
    REQUIRE(deserialized.param_int == data.param_int);
    REQUIRE_THAT(deserialized.param_double,
        Catch::Matchers::WithinRel(data.param_double));
    REQUIRE(deserialized.binary == data.binary);

    BENCHMARK("deserialize struct with rapidjson") {
        return deserialize(serialized);
    };
}
