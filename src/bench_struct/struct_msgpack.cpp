#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <msgpack.hpp>

#include "bench_struct/common.h"
#include "msgpack_helper/nocopy_reference_func.h"

namespace msgpack {
namespace adaptor {

template <>
struct pack<test_struct> {
    template <typename Stream>
    msgpack::packer<Stream>& operator()(
        msgpack::packer<Stream>& out, const test_struct& data) const {
        out.pack(std::forward_as_tuple(
            data.param_int, data.param_double, data.binary));
        return out;
    }
};

template <>
struct as<test_struct> {
    test_struct operator()(const msgpack::object& obj) const {
        auto data =
            obj.as<std::tuple<std::int32_t, double, std::vector<char>>>();
        return test_struct{
            std::get<0>(data), std::get<1>(data), std::move(std::get<2>(data))};
    }
};

}  // namespace adaptor
}  // namespace msgpack

TEST_CASE("bench_struct_msgpack") {
    const auto data = create_data();

    BENCHMARK("serialize struct with msgpack-c") {
        msgpack::sbuffer buf;
        msgpack::pack(buf, data);
        const auto serialized =
            std::vector<char>(buf.data(), buf.data() + buf.size());
        return serialized;
    };

    msgpack::sbuffer buf;
    msgpack::pack(buf, data);
    const auto serialized =
        std::vector<char>(buf.data(), buf.data() + buf.size());

    msgpack::null_visitor visitor;
    REQUIRE_NOTHROW(
        msgpack::parse(serialized.data(), serialized.size(), visitor));

    BENCHMARK("parse struct with msgpack-c") {
        msgpack::null_visitor visitor;
        msgpack::parse(serialized.data(), serialized.size(), visitor);
    };

    const auto obj_handle = msgpack::unpack(
        serialized.data(), serialized.size(), nocopy_reference_func);
    const auto deserialized = obj_handle->as<test_struct>();
    REQUIRE(deserialized.param_int == data.param_int);
    REQUIRE(deserialized.param_double == data.param_double);
    REQUIRE(deserialized.binary == data.binary);

    BENCHMARK("deserialize struct with msgpack-c") {
        const auto obj_handle = msgpack::unpack(
            serialized.data(), serialized.size(), nocopy_reference_func);
        const auto deserialized = obj_handle->as<test_struct>();
        return deserialized;
    };
}
