#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <nlohmann/json.hpp>

#include "bench_string/common.h"

TEST_CASE("bench_string_nlohmann_json") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_string(size);

    BENCHMARK("serialize string(" + size_str + ") with nlohmann/json") {
        return nlohmann::json(data).dump();
    };

    const auto serialized = nlohmann::json(data).dump();

    const auto deserialized =
        nlohmann::json::parse(serialized).get<std::string>();
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize string(" + size_str + ") with nlohmann/json") {
        return nlohmann::json::parse(serialized).get<std::string>();
    };
}
