#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <nlohmann/json.hpp>
#include <simdjson.h>

#include "bench_string/common.h"

TEST_CASE("bench_string_simdjson") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_string(size);

    // simdjson cannot create JSON string
    // (https://github.com/simdjson/simdjson/issues/821#issuecomment-620208285).
    const auto serialized_raw = nlohmann::json(data).dump();
    const auto serialized = simdjson::padded_string(serialized_raw);

    simdjson::ondemand::parser parser;
    auto deserialized_doc = parser.iterate(serialized);
    REQUIRE(static_cast<std::string>(
                static_cast<std::string_view>(deserialized_doc)) == data);

    BENCHMARK("deserialize string(" + size_str + ") with simdjson") {
        simdjson::ondemand::parser parser;
        auto deserialized_doc = parser.iterate(serialized);
        return static_cast<std::string_view>(deserialized_doc);
    };
}
