#include <string_view>

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "bench_data/bench_data.pb.h"
#include "bench_string/common.h"

TEST_CASE("bench_string_protobuf") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_string(size);

    BENCHMARK("serialize string(" + size_str + ") with protobuf") {
        bench_data::StringData buf;
        buf.set_data(data);
        const auto serialized = buf.SerializeAsString();
        return serialized;
    };

    bench_data::StringData pack_buf;
    pack_buf.set_data(data);
    const auto serialized = pack_buf.SerializeAsString();

    bench_data::StringData unpack_buf;
    REQUIRE(unpack_buf.ParseFromString(serialized));
    const auto deserialized = unpack_buf.data();
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize string(" + size_str + ") with protobuf") {
        bench_data::StringData buf;
        buf.ParseFromString(serialized);
        const std::string_view deserialized = buf.data();
        return deserialized;
    };
}
