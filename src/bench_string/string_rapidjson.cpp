#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "bench_string/common.h"

TEST_CASE("bench_string_rapidjson") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_string(size);

    BENCHMARK("serialize string(" + size_str + ") with rapidjson") {
        rapidjson::Document doc;
        doc.SetString(data.data(), data.size());
        rapidjson::StringBuffer buf;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
        doc.Accept(writer);
        const auto serialized = buf.GetString();
        return serialized;
    };

    rapidjson::Document pack_doc;
    pack_doc.SetString(data.data(), data.size());
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
    pack_doc.Accept(writer);
    const auto serialized = std::string(buf.GetString());

    rapidjson::Document unpack_doc;
    unpack_doc.Parse(serialized.data());
    REQUIRE(unpack_doc.IsString());
    const auto deserialized = std::string(unpack_doc.GetString());
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize string(" + size_str + ") with rapidjson") {
        rapidjson::Document doc;
        doc.Parse(serialized.data());
        if (!doc.IsString()) {
            // rapidjson can't check this
            throw std::runtime_error("string is required");
        }
        const auto deserialized = std::string(doc.GetString());
        return deserialized;
    };
}
