#include <string_view>

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <msgpack.hpp>

#include "bench_string/common.h"
#include "msgpack_helper/nocopy_reference_func.h"

TEST_CASE("bench_string_msgpack") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_string(size);

    BENCHMARK("serialize string(" + size_str + ") with msgpack-c") {
        msgpack::sbuffer buf;
        msgpack::pack(buf, data);
        const auto serialized =
            std::vector<char>(buf.data(), buf.data() + buf.size());
        return serialized;
    };

    msgpack::sbuffer buf;
    msgpack::pack(buf, data);
    const auto serialized =
        std::vector<char>(buf.data(), buf.data() + buf.size());

    msgpack::null_visitor visitor;
    REQUIRE_NOTHROW(
        msgpack::parse(serialized.data(), serialized.size(), visitor));

    BENCHMARK("parse string(" + size_str + ") with msgpack-c") {
        msgpack::null_visitor visitor;
        msgpack::parse(serialized.data(), serialized.size(), visitor);
    };

    const auto obj_handle = msgpack::unpack(
        serialized.data(), serialized.size(), nocopy_reference_func);
    const auto deserialized = obj_handle->as<std::string>();
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize string(" + size_str + ") with msgpack-c") {
        const auto obj_handle = msgpack::unpack(
            serialized.data(), serialized.size(), nocopy_reference_func);
        const auto deserialized = obj_handle->as<std::string_view>();
        return deserialized;
    };
}
