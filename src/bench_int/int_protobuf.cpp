#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "bench_data/bench_data.pb.h"
#include "bench_int/common.h"

TEST_CASE("bench_int_protobuf") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_int(size);

    BENCHMARK("serialize int[" + size_str + "] with protobuf") {
        bench_data::IntData buf;
        buf.mutable_data()->Reserve(data.size());
        for (const auto val : data) {
            buf.mutable_data()->AddAlreadyReserved(val);
        }
        const auto serialized = buf.SerializeAsString();
        return serialized;
    };

    bench_data::IntData pack_buf;
    pack_buf.mutable_data()->Reserve(data.size());
    for (const auto val : data) {
        pack_buf.mutable_data()->AddAlreadyReserved(val);
    }
    const auto serialized = pack_buf.SerializeAsString();

    bench_data::IntData unpack_buf;
    REQUIRE(unpack_buf.ParseFromString(serialized));
    const auto deserialized =
        std::vector<int>(unpack_buf.data().begin(), unpack_buf.data().end());
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize int[" + size_str + "] with protobuf") {
        bench_data::IntData buf;
        REQUIRE(buf.ParseFromString(serialized));
        const auto deserialized =
            std::vector<int>(buf.data().begin(), buf.data().end());
        return deserialized;
    };
}
