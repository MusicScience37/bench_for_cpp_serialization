#include <sstream>

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>

#include "bench_int/common.h"

TEST_CASE("bench_int_cereal") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_int(size);

    BENCHMARK("serialize int[" + size_str + "] with cereal") {
        std::ostringstream output_stream;
        {
            cereal::BinaryOutputArchive output_archive(output_stream);
            output_archive(data);
        }
        return output_stream.str();
    };

    std::ostringstream output_stream;
    {
        cereal::BinaryOutputArchive output_archive(output_stream);
        output_archive(data);
    }
    const auto serialized = output_stream.str();

    std::istringstream input_stream;
    input_stream.str(serialized);
    std::vector<int> deserialized;
    {
        cereal::BinaryInputArchive input_archive(input_stream);
        input_archive(deserialized);
    }
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize int[" + size_str + "] with cereal") {
        std::istringstream input_stream;
        input_stream.str(serialized);
        std::vector<int> deserialized;
        {
            cereal::BinaryInputArchive input_archive(input_stream);
            input_archive(deserialized);
        }
        return deserialized;
    };
}
