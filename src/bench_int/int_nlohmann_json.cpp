#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <nlohmann/json.hpp>

#include "bench_int/common.h"

TEST_CASE("bench_int_nlohmann_json") {
    const auto size = GENERATE(1, 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_int(size);

    BENCHMARK("serialize int[" + size_str + "] with nlohmann/json") {
        return nlohmann::json(data).dump();
    };

    const auto serialized = nlohmann::json(data).dump();

    const auto deserialized =
        nlohmann::json::parse(serialized).get<std::vector<int>>();
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize int[" + size_str + "] with nlohmann/json") {
        return nlohmann::json::parse(serialized).get<std::vector<int>>();
    };
}
