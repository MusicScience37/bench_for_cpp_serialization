#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

#include "bench_data/bench_data.pb.h"
#include "bench_double/common.h"

TEST_CASE("bench_double_protobuf") {
    const auto size = GENERATE(1, 1024, 1024 * 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_double(size);

    BENCHMARK("serialize double[" + size_str + "] with protobuf") {
        bench_data::DoubleData buf;
        buf.mutable_data()->Reserve(data.size());
        for (const auto val : data) {
            buf.mutable_data()->AddAlreadyReserved(val);
        }
        const auto serialized = buf.SerializeAsString();
        return serialized;
    };

    bench_data::DoubleData pack_buf;
    pack_buf.mutable_data()->Reserve(data.size());
    for (const auto val : data) {
        pack_buf.mutable_data()->AddAlreadyReserved(val);
    }
    const auto serialized = pack_buf.SerializeAsString();

    bench_data::DoubleData unpack_buf;
    REQUIRE(unpack_buf.ParseFromString(serialized));
    const auto deserialized =
        std::vector<double>(unpack_buf.data().begin(), unpack_buf.data().end());
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize double[" + size_str + "] with protobuf") {
        bench_data::DoubleData buf;
        REQUIRE(buf.ParseFromString(serialized));
        const auto deserialized =
            std::vector<double>(buf.data().begin(), buf.data().end());
        return deserialized;
    };
}
