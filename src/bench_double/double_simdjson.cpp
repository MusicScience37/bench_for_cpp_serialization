#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <nlohmann/json.hpp>
#include <simdjson.h>

#include "bench_double/common.h"

TEST_CASE("bench_double_simdjson") {
    const auto size = GENERATE(1, 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_double(size);

    // simdjson cannot create JSON string
    // (https://github.com/simdjson/simdjson/issues/821#issuecomment-620208285).
    const auto serialized_raw = nlohmann::json(data).dump();
    const auto serialized = simdjson::padded_string(serialized_raw);

    simdjson::ondemand::parser parser;
    auto deserialized_doc = parser.iterate(serialized);
    std::vector<double> deserialized;
    deserialized.reserve(deserialized_doc.count_elements());
    for (auto elem : deserialized_doc.get_array()) {
        deserialized.push_back(static_cast<double>(elem));
    }
    REQUIRE(deserialized == data);

    BENCHMARK("deserialize double[" + size_str + "] with simdjson") {
        simdjson::ondemand::parser parser;
        auto deserialized_doc = parser.iterate(serialized);
        std::vector<double> deserialized;
        deserialized.reserve(deserialized_doc.count_elements());
        for (auto elem : deserialized_doc.get_array()) {
            deserialized.push_back(static_cast<double>(elem));
        }
        return deserialized;
    };
}
