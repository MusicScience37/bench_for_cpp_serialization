#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include "bench_double/common.h"

TEST_CASE("bench_double_rapidjson") {
    const auto size = GENERATE(1, 1024);
    const auto size_str = std::to_string(size);
    const auto data = create_double(size);

    BENCHMARK("serialize double[" + size_str + "] with rapidjson") {
        rapidjson::Document doc;
        doc.SetArray();
        doc.Reserve(data.size(), doc.GetAllocator());
        for (const auto val : data) {
            doc.PushBack(val, doc.GetAllocator());
        }
        rapidjson::StringBuffer buf;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
        doc.Accept(writer);
        const auto serialized = buf.GetString();
        return serialized;
    };

    rapidjson::Document pack_doc;
    pack_doc.SetArray();
    pack_doc.Reserve(data.size(), pack_doc.GetAllocator());
    for (const auto val : data) {
        pack_doc.PushBack(val, pack_doc.GetAllocator());
    }
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
    pack_doc.Accept(writer);
    const auto serialized = std::string(buf.GetString());

    rapidjson::Document unpack_doc;
    unpack_doc.Parse(serialized.data());
    REQUIRE(unpack_doc.IsArray());
    std::vector<double> deserialized;
    deserialized.reserve(unpack_doc.Size());
    for (auto iter = unpack_doc.Begin(); iter != unpack_doc.End(); ++iter) {
        REQUIRE(iter->IsDouble());
        deserialized.push_back(iter->GetDouble());
    }
    REQUIRE_THAT(deserialized, Catch::Matchers::Approx(data));

    BENCHMARK("deserialize double[" + size_str + "] with rapidjson") {
        rapidjson::Document doc;
        doc.Parse(serialized.data());
        if (!doc.IsArray()) {
            // rapidjson can't check this
            throw std::runtime_error("array is required");
        }
        std::vector<double> deserialized;
        deserialized.reserve(doc.Size());
        for (auto iter = doc.Begin(); iter != doc.End(); ++iter) {
            if (!iter->IsDouble()) {
                // rapidjson can't check this
                throw std::runtime_error("double value is required");
            }
            deserialized.push_back(iter->GetDouble());
        }
        return deserialized;
    };
}
