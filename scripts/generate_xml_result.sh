#!/bin/bash

set -e

cd $(dirname $0)/..

BIN_DIR=./build/Release/bin

${BIN_DIR}/bench_string --reporter xml -o bench_string.xml
${BIN_DIR}/bench_int --reporter xml -o bench_int.xml
${BIN_DIR}/bench_double --reporter xml -o bench_double.xml
${BIN_DIR}/bench_struct --reporter xml -o bench_struct.xml
